import tkinter as tk
from tkinter import filedialog
import pandas as pd
from src import dmgui, dmdata


class App(dmgui.GraphWindow):
    """Main application's class for storing, processing and visualization spatial data via application's window.

    Methods
    -------
    config_menu()
        Configure menu
    load_data()
        Load and process data
    save_report()
        Save report to text file
    save_graph()
        Save graph to PNG file
    create_report()
        Generate report
    open_instruction()
        Open window with polish instruction
    open_about()
        Open window with information about application
    """

    def __init__(self, title):
        """
        :param title: str
        """
        dmgui.GraphWindow.__init__(self, title)

        # Initial data container
        self.__data = dmdata.Data()

        # Configure menu
        self.config_menu()

    def config_menu(self):
        """
        Configure menu
        """

        # Add file menu options
        self.menubar.filemenu.add_command(label='Open file...', command=self.load_data)
        self.menubar.filemenu.add_command(label='Export report...', command=self.save_report)
        self.menubar.filemenu.add_command(label='Export graph...', command=self.save_graph)

        # Add help menu options
        self.menubar.helpmenu.add_command(label='Instruction PL', command=self.open_instruction)
        self.menubar.helpmenu.add_command(label='About', command=self.open_about)

    def load_data(self):
        """
        Load and process data
        """

        path = filedialog.askopenfilename(initialdir='/', title='Select file', filetypes=(('text files', '*.txt'), ('all files', '*.*')))
        if path:
            self.__data.loaddatafromfile(path)
            self.__data.computedata()
            self.__data.draw(self)
            self.create_report()

    def save_report(self):
        """
        Save report to text file
        """
        file = filedialog.asksaveasfile(mode='w', defaultextension='.txt', initialdir='/', title='Save report as',
                                        filetypes=[('text files', '*.txt')])
        if file:
            file.write(self.log_frame.logbox.get('1.0', tk.END))
            file.close()

    def save_graph(self):
        """
        Save graph to PNG file
        """
        path = filedialog.asksaveasfilename(defaultextension='.png',
                                            initialdir='/', title='Save graph as', filetypes=[('PNG', '*.png')])
        if path:
            self.graph_frame.fig.savefig(path)

    def create_report(self):
        """
        Generate report
        """

        # Set grater maximum column width for displaying pandas DataFrames
        pd.set_option('display.max_colwidth', 250)

        # Create tag coordinates table
        tagdata = {'tag_id': pd.Series([point.name for point in self.__data.get_tags()]),
                   'x': pd.Series([point.geometry.x for point in self.__data.get_tags()]).astype(float),
                   'y': pd.Series([point.geometry.y for point in self.__data.get_tags()]).astype(float),
                   'x_err': pd.Series([point.accuracy[0] for point in self.__data.get_tags()]).astype(float),
                   'y_err': pd.Series([point.accuracy[1] for point in self.__data.get_tags()]).astype(float)
                   }
        tagstable = pd.DataFrame(tagdata)

        # Create cross tags id and polygons table
        tagspolygonsdata = {'tag_id': dmdata.Tag.get_unique_id(self.__data.get_tags())}
        tagspolygonstable = pd.DataFrame(tagspolygonsdata)

        # Tags total by tags id
        pointsnumber = []
        for pointname in dmdata.Tag.get_unique_id(self.__data.get_tags()):
            pointsnumber.append(len([point for point in self.__data.get_tags() if point.name == pointname]))
        tagspolygonstable['total'] = pd.Series(pointsnumber)

        # Tags in polygons by tags id
        pointsoutsidepolygons = set(self.__data.get_tags())
        for reg in self.__data.get_regions():
            pointsnumber = []
            for pointname in dmdata.Tag.get_unique_id(self.__data.get_tags()):
                pointsnumber.append(len([point for point in reg.inner_points if point.name == pointname]))
            tagspolygonstable['polygon_'+reg.name] = pd.Series(pointsnumber)
            pointsoutsidepolygons -= reg.inner_points

        # Tags outside polygons by tags id
        pointsnumber = []
        for pointname in dmdata.Tag.get_unique_id(self.__data.get_tags()):
            pointsnumber.append(len([point for point in pointsoutsidepolygons if point.name == pointname]))
        tagspolygonstable['outside'] = pd.Series(pointsnumber)
        tagspolygonstable.set_index('tag_id', inplace=True)

        # Table of tags id with maximum occurrence in polygons
        maxtagtable = pd.DataFrame(tagspolygonstable.idxmax(axis=0), columns=['tag_id'])

        # Table of tags for each polygon
        innerpointsdata_lvl0 = {'polygon_id': pd.Series([poly.name for poly in self.__data.get_regions()]),
                                'inner_tags_id': pd.Series([dmdata.Tag.get_unique_id(poly.inner_points) for poly in self.__data.get_regions()]),
                                'tags_number': pd.Series([len(poly.inner_points) for poly in self.__data.get_regions()])
                                }
        innerpointstable = pd.DataFrame(innerpointsdata_lvl0)
        innerpointstable = innerpointstable.append({'polygon_id': 'none',
                                                    'inner_tags_id': dmdata.Tag.get_unique_id(pointsoutsidepolygons),
                                                    'tags_number': str(len(pointsoutsidepolygons))
                                                    }, ignore_index=True)

        # Add data to string report
        report = 'Found ' + str(len(self.__data.get_tags())) + ' points'
        report += '\nFound ' + str(len(self.__data.get_regions())) + ' polygons'
        report += '\n\n\n1. Tag coordinates'
        report += '\n\n' + tagstable.to_string(index=False, float_format='%.3f')
        report += '\n\n\n2. Tags in polygons'
        report += '\n\n' + innerpointstable.to_string(index=False)

        # Table of tags in polygon's intersections, if exist
        if any([len(poly.intersected_polygons) for poly in self.__data.get_regions()]):
            iptable_1 = pd.DataFrame()
            for reg in self.__data.get_regions():
                for innerpolygon in reg.intersected_polygons:
                    iptable_1 = iptable_1.append({'area': str(reg.name)+'_without_'+str(innerpolygon.name),
                                                  'inner_tags_id': dmdata.Tag.get_unique_id(reg.inner_points - innerpolygon.inner_points),
                                                  'tags_number': str(len(reg.inner_points - innerpolygon.inner_points))
                                                  }, ignore_index=True)
                    iptable_1 = iptable_1.append({'area': str(reg.name)+'_and_'+str(innerpolygon.name),
                                                  'inner_tags_id': dmdata.Tag.get_unique_id(reg.inner_points & innerpolygon.inner_points),
                                                  'tags_number': str(len(reg.inner_points & innerpolygon.inner_points))
                                                  }, ignore_index=True)
            report += '\n\n\n2.1 Tags in parts of polygons'
            report += '\n\n' + iptable_1.to_string(index=False)

        # Add other data to string report
        report += '\n\n\n3. Tags in polygons - details'
        report += '\n\n' + tagspolygonstable.to_string()
        report += '\n\n\n4. Tags with maximum occurrence'
        report += '\n\n' + maxtagtable.to_string()

        # Insert report text to textbox
        self.log_frame.logbox.config(state=tk.NORMAL)
        self.log_frame.logbox.delete('1.0', tk.END)
        self.log_frame.logbox.insert(tk.END, report)
        self.log_frame.logbox.config(state=tk.DISABLED)

    def open_instruction(self):
        """
        Open window with polish instruction
        """

        with open('help\\instruction_PL.txt', 'r', encoding='utf8') as instructionfile:
            instructiontext = instructionfile.read()
        instructionwindow = dmgui.TextWindow(self, 80, 25, 'Instruction')
        instructionwindow.add_text(instructiontext)
        instructionwindow.mainloop()

    def open_about(self):
        """
        Open window with information about application
        """
        with open('help\\about.txt', 'r', encoding='utf8') as aboutfile:
            text = aboutfile.read()
        aboutwindow = dmgui.TextWindow(self, 60, 20, 'About...')
        aboutwindow.add_text(text)
        aboutwindow.mainloop()


# if __name__ == '__main__':
#     app = App('Indoorway - zadanie 2. - Marczykowska')
#     app.mainloop()
