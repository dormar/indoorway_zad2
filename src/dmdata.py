import numpy
import math
import copy
from sympy import Polygon, Point
import matplotlib.pyplot as plt
import matplotlib.patches as patches


class Tag:
    """Class for tags data like name, position, accuracy.
    This class provides method to determine position based on distances from known points.

    Attributes
    ----------
    name : int
        Tag id
    geometry : Point
        The geometry of tag
    accuracy : [mx, my]
        Accuracy of tag position

    Methods
    -------
    distance(p1, p2)
        static method
        Calculate distance between tags t1 and t2
    calculate(r, anchors)
        Determine position based on distances r to anchors
    get_unique_id(tags)
        Static method to get list of unique names for set of tags
    """

    def __init__(self, name, x=0.0, y=0.0):
        """
        :param name: int
        :param x: float
        :param y: float
        """
        self.name = name
        self.geometry = Point(x, y, evaluate=False)
        self.accuracy = 0.0

    def __str__(self):
        return str(self.name) + ' [' + str(self.geometry.x) + ', ' + str(self.geometry.y) + ']'

    def __repr__(self):
        return str(self)

    @staticmethod
    def distance(p1, p2):
        """Returns euclidean distance between tags.

        :param p1: Tag
        :param p2: Tag
        :return: float
        """
        return float(p1.geometry.distance(p2.geometry))

    def __omega(self, d, anchor):
        """Returns difference between given distance d and euclidean distance between Tag and anchor tag.

        :param d: float
        :param anchor: Tag
        :return: float
        """
        return d - Tag.distance(self, anchor)

    def __ux(self, anchor):
        """Return coefficient for x coordinate in linearized equation of distance to anchor observation.

        :param anchor: Tag
        :return: float
        """
        return - (float(anchor.geometry.x - self.geometry.x)) / float(Tag.distance(self, anchor))

    def __uy(self, anchor):
        """Return coefficient for y coordinate in linearized equation of distance to anchor observation.

        :param anchor: Tag
        :return: float
        """
        return - float(anchor.geometry.y - self.geometry.y) / float(Tag.distance(self, anchor))

    def calculate(self, r, anchors):
        """Determine position based on distances r to anchors.

        :param r: list of floats
        :param anchors: list of Tag
        """

        numiter = 0
        while True:
            old_pos = copy.copy(self)  # Odl position to check convergence of the algorithm
            numiter += 1

            # Free vector
            l_arr = []
            for i in range(len(r)):
                l_arr.append([self.__omega(r[i], anchors[i])])
            L = numpy.matrix(l_arr)  # Matrix of differences between observed and estimated distances

            # Matrix of linearized observation equations
            g_arr = []
            for i in range(len(r)):
                g_arr.append([self.__ux(anchors[i]), self.__uy(anchors[i])])
            G = numpy.matrix(g_arr)

            # System of normal equations
            GTL = numpy.matmul(G.transpose(), L)
            GTG_1 = numpy.linalg.inv(numpy.matmul(G.transpose(), G))

            # Estimated parameters (increments to coordinates)
            X = numpy.matmul(GTG_1, GTL)

            # Determined coordinates
            self.geometry = Point(self.geometry.x + float(X[0]), self.geometry.y + float(X[1]), evaluate=False)

            if Tag.distance(old_pos, self) < 0.0001 or numiter >= 2:
                break

        V = numpy.matmul(G, X) - L  # Vector of observations (distances) corrections
        m0 = math.sqrt(float(numpy.matmul(V.transpose(), V)) / (len(r) - 2))  # Mean error of observations
        Cx = m0 ** 2 * GTG_1  # Increment estimator covariance matrix
        self.accuracy = numpy.sqrt([Cx[0, 0], Cx[1, 1]])

    @staticmethod
    def get_unique_id(tags):
        """Return array of unique id for set of tags.

        :param tags: set of Tag
        :return: array
        """
        return numpy.unique(numpy.sort(numpy.array([tag.name for tag in tags])))


class Region:
    """Class for polygon data like name, geometry, tags inside polygon.

    Attributes
    ----------
    name : int
        Tag id
    geometry : Polygon
        The geometry of region
    vertices : np.array
        array of coordinates of vertices
    inner_points : set of Tag
        set of tags inside polygon
    intersected_polygons : set of Region
        set of regions which are intersected or inside that region

    Methods
    -------
    tags_inside(tags)
        Determine which tags are inside region
    is_intersected(region)
        Check if given region is intersected or inside the region
    determine_regions_intersections(regions)
        Determine which regions are intersected or inside the region
    get_unique_interior_points_id()
        Get list of unique names for tags inside the region
    """
    def __init__(self, name, coords):
        """
        :param name: int
        :param coords: tuple of Points
        """
        self.name = name
        self.geometry = Polygon(*coords, evaluate=False)
        self.vertices = numpy.asarray(coords)
        self.inner_points = set()
        self.intersected_polygons = set()

    def __str__(self):
        return self.name + ' ' + str(self.geometry)

    def __repr__(self):
        return str(self)

    def tags_inside(self, tags):
        """
        Determine which tags are inside region
        :param tags: set of Tag
        """

        # Clear set of inner tags
        self.inner_points = set()

        # Reduce number of searched tags to polygon's bbox area
        (minx, miny, maxx, maxy) = self.geometry.bounds
        tagsinbbox = set([tag for tag in tags if minx < tag.geometry.x < maxx and miny < tag.geometry.y < maxy])

        for tag in tagsinbbox:
            if self.geometry.encloses(tag.geometry):
                self.inner_points.add(tag)

    def is_intersected(self, region):
        """
        Check if given region is intersected or inside the region
        :param region: Region
        :return: bool
        """

        # Reduce number of vertices to polygon's bbox area
        (minx, miny, maxx, maxy) = self.geometry.bounds
        verticesinbbox = [ver for ver in region.geometry.vertices if minx < ver.x < maxx and miny < ver.y < maxy]

        # Check if any vertices of second region is inside of the region
        for v in verticesinbbox:
            if self.geometry.encloses_point(v):
                return True
        return False

    def determine_regions_intersections(self, regions):
        """
        Determine which regions are intersected or inside the region
        :param regions: set of Region
        """
        for region in regions:
            if self.is_intersected(region):
                self.intersected_polygons.add(region)

    def get_unique_interior_points_id(self):
        """
        Get list of unique names for tags inside the region
        :return: array
        """
        return Tag.get_unique_id(self.inner_points)


class HoveringAnnotation:
    """Scatter annotation which appears when mouse hover over object of scatter.

    Attributes
    ----------
    annot : matplotlib.text.Annotation
        Hovering annotations
    window : dmgui.GraphWindow
        Window where scatter is plotted
    plot : matplotlib.collections.PathCollection
        Scatter plot
    labels : list
        Content of annotations

    Methods
    -------
    update_annot(ind)
        Update annotations content
    hover(event)
        Callback for hovering over scatter object
    """
    def __init__(self, window, plot, labels):
        self.annot = window.get_ax().annotate("", xy=(0, 0), xytext=(0, 10), textcoords='offset points',
                                              bbox=dict(boxstyle='round', fc='w'))
        self.annot.set_visible(False)
        self.window = window
        self.plot = plot
        self.labels = labels

        # Connect hover callback to axes of window
        self.window.get_figure().canvas.mpl_connect('motion_notify_event', lambda event: self.hover(event))

    def update_annot(self, ind):
        pos = self.plot.get_offsets()[ind['ind'][0]]  # position of hovering mouse
        self.annot.xy = pos  # update annotate position
        text = ', '.join([str(self.labels[n]) for n in ind['ind']])  # list of labels as a string for objects under the mause
        self.annot.set_text(text)  # update annotate text
        self.annot.get_bbox_patch().set_alpha(0.5)

    def hover(self, event):
        # Show annotation if mouse is hovering over object and hide if not
        vis = self.annot.get_visible()
        if event.inaxes == self.window.get_ax():
            cont, ind = self.plot.contains(event)
            if cont:
                self.update_annot(ind)
                self.annot.set_visible(True)
                self.window.get_figure().canvas.draw()
            else:
                if vis:
                    self.annot.set_visible(False)
                    self.window.get_figure().canvas.draw()


class Data:
    """Class to store, determine and draw sets of tags, anchors and regions.

    Methods
    -------
    loaddatafromfile(inputpath)
        Load data from text file and sort it by names
    computedata()
        Determine position for each tag. Determine regions intersections and tags in regions.
    get_regions()
        Get regions
    get_anchors()
        Get anchors
    get_tags()
        Get tags
    draw(window)
        Draw data on the graph in the window
    """

    def __init__(self):
        self.__tags = []
        self.__distances = []
        self.__anchors = []
        self.__polygons_description = []
        self.__polygons = []
        self.__nodes = []

    def loaddatafromfile(self, inputpath):
        """
        Load data from text file and sort it by names
        :param inputpath: str
        """

        # Clear data
        self.__tags = []
        self.__distances = []
        self.__anchors = []
        self.__polygons_description = []
        self.__polygons = []
        self.__nodes = []

        with open(inputpath, 'r') as file:
            line = file.readline()

            while line:
                if len(line.strip(' \t\n\r')) != 0:
                    # Read anchors as Tag objects
                    if line.split()[0] == 'anchor_id':
                        line = file.readline()
                        blank = 0
                        while line:
                            if len(line.strip(' \t\n\r')) != 0:
                                data = line.split()
                                self.__anchors.append(Tag(data[0], float(data[1]), float(data[2])))
                                blank = 0
                            else:
                                blank += 1
                            line = file.readline()
                            #  break on 2 empty lines
                            if blank > 1:
                                break
                    #  Read Tags and measured distances to anchors
                    elif line.split()[0] == 'tag_id':
                        line = file.readline()
                        blank = 0
                        while line:
                            if len(line.strip(' \t\n\r')) != 0:
                                data = line.split()
                                self.__tags.append(Tag(int(data[0])))
                                self.__distances.append([float(r) for r in data[1:]])
                                blank = 0
                            else:
                                blank += 1
                            line = file.readline()
                            #  break on 2 empty lines
                            if blank > 1:
                                break
                    # Read polygons vertices
                    elif line.split()[0] == 'poly_id':
                        line = file.readline()
                        blank = 0
                        while line:
                            if len(line.strip(' \t\n\r')) != 0:
                                data = line.split(' ', 1)
                                self.__polygons_description.append(
                                    [data[0], [int(node) for node in data[1].replace(']', '').lstrip('[').split()]])
                                blank = 0
                            else:
                                blank += 1
                            line = file.readline()
                            #  break on 2 empty lines
                            if blank > 1:
                                break
                    # Read vertices a Tag objects
                    elif line.split()[0] == 'node_id':
                        line = file.readline()
                        blank = 0
                        while line:
                            if len(line.strip(' \t\n\r')) != 0:
                                data = line.split()
                                self.__nodes.append(Tag(data[0], float(data[1]), float(data[2])))
                                blank = 0
                            else:
                                blank += 1
                            line = file.readline()
                            #  break on 2 empty lines
                            if blank > 1:
                                break
                line = file.readline()
        file.close()

        # Create Region objects based on vertices names
        for poly in self.__polygons_description:
            poly_nodes = []
            for nodeName in poly[1]:
                for p in self.__nodes:
                    if int(p.name) == int(nodeName):
                        poly_nodes.append(p.geometry)
                        break
            self.__polygons.append(Region(poly[0], tuple(poly_nodes)))

        # Sort tags and distances by tags names
        if 0 not in (len(self.__tags), len(self.__distances), len(self.__anchors)):
            s = sorted(zip(self.__tags, self.__distances), key=lambda tag: tag[0].name)
            self.__tags, self.__distances = map(list, zip(*s))

        # Sort polygons by names
        if len(self.__polygons) != 0:
            self.__polygons = sorted(self.__polygons, key=lambda reg: reg.name)

    def computedata(self):
        """
        Determine position for each tag. Determine regions intersections and tags in regions.
        """

        for i, tag in enumerate(self.__tags):
            tag.calculate(self.__distances[i], self.__anchors)

        for reg in self.__polygons:
            reg.determine_regions_intersections(self.__polygons)
            reg.tags_inside(self.__tags)

    def get_regions(self):
        """
        Get regions
        :return: list of Region
        """
        return self.__polygons

    def get_tags(self):
        """
        Get tags
        :return: list of Tag
        """
        return self.__tags

    def get_anchors(self):
        """
        Get anchors
        :return: list of Tag
        """
        return self.__anchors

    def draw(self, window):
        """
        Draw data on the graph in the window
        :param window: GraphWindow
        """

        ax = window.get_ax()
        ax.cla()  # Clear axes

        # Draw polygons
        cmap = plt.get_cmap('jet')
        for r, reg in enumerate(self.__polygons):
            ax.add_patch(patches.Polygon(reg.vertices, True, facecolor='none', edgecolor=cmap(r / len(self.__polygons)),
                                         linewidth=2, label='Polygon '+reg.name))

        # Draw anchors
        anchorsx = [anchor.geometry.x for anchor in self.__anchors]
        anchorsy = [anchor.geometry.y for anchor in self.__anchors]
        anchorscatter = ax.scatter(anchorsx, anchorsy, 50, color=[0.0, 0.61, 0.76], marker='^', zorder=2, label='Anchor')
        anchorslabel = ['A' + anchor.name for anchor in self.__anchors]
        HoveringAnnotation(window, anchorscatter, anchorslabel)

        # Draw tags
        tagsx = [tag.geometry.x for tag in self.__tags]
        tagsy = [tag.geometry.y for tag in self.__tags]
        tagslabel = [tag.name for tag in self.__tags]
        tagscatter = ax.scatter(tagsx, tagsy, 10, color=[0.0, 0.71, 0.86], zorder=2, label='Tag')
        HoveringAnnotation(window, tagscatter, tagslabel)

        # Create accuracy ellipses
        circlepatches = []
        for tag in self.__tags:
            ellipse = patches.Ellipse((tag.geometry.x, tag.geometry.y), tag.accuracy[0] * 2, tag.accuracy[1] * 2,
                                      alpha=0.2, color=[0.0, 0.71, 0.86], zorder=0)
            circlepatches.append(ellipse)
        window.set_accuracyplot(circlepatches)

        # Create and place legend
        chartbox = ax.get_position()
        if not window.isgraph:
            ax.set_position([chartbox.x0, chartbox.y0, chartbox.width * 0.8, chartbox.height])
            ax.legend(loc='upper right', bbox_to_anchor=(1.34, 1), fontsize=9)
        else:
            ax.legend(loc='upper right', bbox_to_anchor=(1.34, 1), fontsize=9)

        # Redraw the graph
        window.redraw_graph()
        window.isgraph = True
