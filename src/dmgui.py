import tkinter as tk
from tkinter import ttk
import matplotlib.ticker
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
matplotlib.use('TkAgg')


class GraphWindow(tk.Tk):
    """Window with graph, text viewer and simple menu.

    Attributes
    ----------
    menubar : MenuBar
        Simple menu with file menu and help
    graph_frame : GraphFrame
        Frame with graph
    log_frame : LogFrame
        Frame with text viewer
    isgraph : bool
        Indicator whether any chart has already been drawn in the window

    Methods
    -------
    get_ax()
        Get graph axes
    get_figure()
        Get figure
    set_accuracyplot(ellipses)
        Set ellipses as accuracyplot for graph_frame
    redraw_graph()
        Redraw graph
    """
    def __init__(self, title):
        """
        :param title: string
        """
        tk.Tk.__init__(self)
        # Set window parameters
        self.resizable(False, False)
        tk.Tk.wm_title(self, title)

        # Create menu bar
        self.menubar = MenuBar(self)
        self.config(menu=self.menubar)

        # Create frame for whole window
        container = tk.Frame(self)
        container.pack(fill=tk.BOTH, expand=0)

        # Create window components
        self.graph_frame = GraphFrame(container, width=600, height=450)
        separator = ttk.Separator(container, orient=tk.VERTICAL)
        self.log_frame = LogFrame(container, boxwidth=40, boxheight=30)

        # Pack window components
        self.graph_frame.pack(side=tk.LEFT, padx=20, pady=20, expand=0)
        separator.pack(side=tk.LEFT, fill=tk.Y, padx=20, pady=20, expand=0)
        self.log_frame.pack(side=tk.RIGHT, fill=tk.Y, padx=10, pady=20, expand=0)

        self.isgraph = False

    def get_ax(self):
        """
        Get axes
        :return: Axes
        """
        return self.graph_frame.get_ax()

    def get_figure(self):
        """
        Get figure
        :return: Figure
        """
        return self.graph_frame.fig

    def set_accuracyplot(self, ellipses):
        """
        Set ellipses as accuracyplot for graph_frame
        :param ellipses: Collection
        """

        if self.graph_frame.accuracyplotvisible:
            self.graph_frame.hide_accuracy()
        self.graph_frame.accuracyplot = ellipses

    def redraw_graph(self):
        """
        Redraw graph
        """
        self.graph_frame.redraw_plot()


class MenuBar(tk.Menu):
    """Simple menu with file menu and help

    Attributes
    ----------
    filemenu : Menu
        Menu "File"
    helpmenu : Menu
        Menu "Help"
    """

    def __init__(self, parent):
        """
        :param parent: Tk
        """
        tk.Menu.__init__(self, parent)
        self.filemenu = tk.Menu(self, tearoff=0)
        self.add_cascade(label='File', menu=self.filemenu)

        self.helpmenu = tk.Menu(self, tearoff=0)
        self.add_cascade(label='Help', menu=self.helpmenu)


class GraphFrame(tk.Frame):
    """Frame with graph and "Show accuracy" button.

    Attributes
    ----------
    fig : Figure
        Figure for graph
    ax : Axes
        Graph axes
    accuracyplot : Collection
        Plot representing accuracy of point data
    accuracyplotvisible : bool
        Indicator whether accuracy plot is drawn in the graph
    canvas : FigureCanvasTkAgg
        Canvas of graph
    button: Button
        Button "Show accuracy"

    Methods
    -------
    show_hide_accuracy()
        Callback to button "Show accuracy" to show or hide accuracy plot
    show_accuracy()
        Show accuracy plot on graph
    hide_accuracy()
        Hide accuracy plot on graph
    get_ax()
        Get graph axes
    redraw_plot()
        Redraw graph
    """

    def __init__(self, parent, width=500, height=350):
        """
        :param parent: Frame
        :param width: int, optional
        :param height: int, optional
        """
        tk.Frame.__init__(self, parent, width=width, height=height)

        # Create figure and add axes
        self.fig = Figure(figsize=(width / 100, height / 100), dpi=100)
        self.ax = self.fig.add_subplot(111)

        # Set axes style
        for axis in [self.ax.xaxis, self.ax.yaxis]:
            axis.grid(True, 'major', ls='solid', lw=0.5, color='gray')
            axis.grid(True, 'minor', ls='solid', lw=0.1, color='gray')
            axis.set_minor_locator(matplotlib.ticker.AutoMinorLocator())
        self.ax.set_axisbelow(True)

        # Initiate accuracyplot
        self.accuracyplot = []
        self.accuracyplotvisible = False

        # Create canvas for figure - embed matplotlib figure in tkinter frame
        self.canvas = FigureCanvasTkAgg(self.fig, self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.TOP, expand=True)

        # Add navigation toolbar to figure - embed matplotlib navigation toolbar in tkinter frame
        toolbar = NavigationToolbar2Tk(self.canvas, self)
        toolbar.update()

        # Create button "Accuracy show"
        self.button = ttk.Button(self, text='Show accuracy', command=lambda: self.show_hide_accuracy())
        self.button.pack(anchor=tk.SE, pady=5)

    def show_hide_accuracy(self):
        """
        Callback to button "Show accuracy" to show or hide accuracy plot
        """
        if self.accuracyplotvisible:
            self.hide_accuracy()
        else:
            self.show_accuracy()

    def show_accuracy(self):
        """
        Show accuracy plot on graph
        """

        if len(self.accuracyplot) != 0:
            for circle in self.accuracyplot:
                self.ax.add_patch(circle)
            self.button['text'] = 'Hide accuracy'
            self.accuracyplotvisible = True
            self.redraw_plot()

    def hide_accuracy(self):
        """
        Hide accuracy plot on graph
        """
        for circle in self.accuracyplot:
            circle.remove()
        self.button['text'] = 'Show accuracy'
        self.accuracyplotvisible = False
        self.redraw_plot()

    def get_ax(self):
        """
        Get graph axes
        :return: Axes
        """
        return self.ax

    def redraw_plot(self):
        """
        Redraw graph
        """

        # Set axes style
        for axis in [self.ax.xaxis, self.ax.yaxis]:
            axis.grid(True, 'major', ls='solid', lw=0.5, color='gray')
            axis.grid(True, 'minor', ls='solid', lw=0.1, color='gray')
            axis.set_minor_locator(matplotlib.ticker.AutoMinorLocator())
        self.ax.set_axisbelow(True)
        # Redraw graph
        self.canvas.draw()


class LogFrame(tk.Frame):
    """Window with text viewer with two scrollbars.

    Attributes
    ----------
    logbox : tk.Text
        Container for text

    Methods
    -------
    print_info(info)
        Print text in text box
    """

    def __init__(self, parent, boxwidth=10, boxheight=30):
        """
        :param parent: Frame
        :param boxwidth: int width of text box as number of characters
        :param boxheight: int height of text box as number of lines
        """
        tk.Frame.__init__(self, parent)

        # Create label "report"
        label = tk.Label(self, text='Report:')
        label.pack(anchor=tk.NW)

        # Create frame for text box and scrollbars
        textftame = tk.Frame(self)
        textftame.pack(side=tk.TOP, padx=5, pady=5)

        # Create scrollbars
        yscrollbar = tk.Scrollbar(textftame)
        yscrollbar.pack(side=tk.RIGHT, padx=5, fill=tk.Y)
        xscrollbar = tk.Scrollbar(textftame, orient=tk.HORIZONTAL)
        xscrollbar.pack(side=tk.BOTTOM, padx=5, fill=tk.X)

        # Create text box
        self.logbox = tk.Text(textftame, width=boxwidth, height=boxheight, state=tk.DISABLED, wrap=tk.NONE)
        self.logbox.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

        # attach logbox to scrollbar
        self.logbox.config(yscrollcommand=yscrollbar.set)
        yscrollbar.config(command=self.logbox.yview)
        self.logbox.config(xscrollcommand=xscrollbar.set)
        xscrollbar.config(command=self.logbox.xview)

    def print_info(self, info):
        """
        Print text in text box
        :param info: str
        """
        self.logbox.config(state=tk.NORMAL)
        self.logbox.insert(tk.END, '\n' + info)
        self.logbox.config(state=tk.DISABLED)


class TextWindow(tk.Toplevel):
    """Window with text viewer with vertical scrollbar.

    Attributes
    ----------
    textbox : tk.Text
        Container for text

    Methods
    -------
    add_text(info)
        Print text in text box
    """
    def __init__(self, parent, boxwidth, boxheight, title):
        """
        :param parent: Frame
        :param boxwidth: int width of text box as number of characters
        :param boxheight: int height of text box as number of lines
        :param title: str
        """
        tk.Toplevel.__init__(self, parent)
        self.title(title)

        # Create frame for text box and scrollbar
        textftame = tk.Frame(self)
        textftame.pack(side=tk.TOP, padx=5, pady=5)

        # Create vertical scrollbar
        yscrollbar = tk.Scrollbar(textftame)
        yscrollbar.pack(side=tk.RIGHT, padx=5, fill=tk.Y)

        # Create text box
        self.textbox = tk.Text(textftame, state=tk.DISABLED, wrap=tk.WORD, width=boxwidth, height=boxheight)
        self.textbox.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

        # attach textbox to scrollbar
        self.textbox.config(yscrollcommand=yscrollbar.set)
        yscrollbar.config(command=self.textbox.yview)

    def add_text(self, info):
        """
        Print text in text box
        :param info: str
        """
        self.textbox.config(state=tk.NORMAL)
        self.textbox.insert(tk.END, '\n' + info)
        self.textbox.config(state=tk.DISABLED)



