# Indoorway - task 2

The application allows you to determine coordinates based on the distance from known points. For given polygons, it also determines which points are on the area of which polygons. The determined locations of the points, their accuracy, polygons and anchors are visualized on the graph. All numerical results are presented in the report.

## Run

To run application run file src\zad2.py

## Requirements
Python 3.6

```python
tkinter
matplotlib
sympy
numpy
pandas
```
## Instruction
The program's manual (only polish version) is located in file src\help\instruction_PL.txt.
## Author
Dorota Marczykowska

## License
It is allowed to use this application free of charge for any purpose, however, it is prohibited to use or modify the source code and its parts.

